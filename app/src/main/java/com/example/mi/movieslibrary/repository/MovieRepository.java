package com.example.mi.movieslibrary.repository;

import android.support.annotation.NonNull;

import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.models.content.Genre;
import com.example.mi.movieslibrary.models.content.Movie;

import java.util.List;

import rx.Observable;

public interface MovieRepository {
    @NonNull
    Observable<List<Movie>> popularMovies();

    @NonNull
    Observable<List<Movie>> topRated();

    @NonNull
    Observable<Movie> movieDetails(int id);

    @NonNull
    Observable<List<Genre>> genres();

    @NonNull
    Observable<List<Cast>> casts(int id);
}
