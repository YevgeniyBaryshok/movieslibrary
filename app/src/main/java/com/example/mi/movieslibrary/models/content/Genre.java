package com.example.mi.movieslibrary.models.content;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

@Entity
public class Genre extends RealmObject  implements Parcelable{
    @PrimaryKey
    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;

    public Genre() {
    }

    public Genre(int mId, String mName) {
        this.mId = mId;
        this.mName = mName;
    }
    public Genre(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(mId);
        parcel.writeString(mName);
    }

    public static final Parcelable.Creator<Genre> CREATOR = new Parcelable.Creator<Genre>() {

        @NonNull
        @Override
        public Genre createFromParcel(Parcel parcel) {
            return new Genre(parcel);
        }

        @NonNull
        @Override
        public Genre[] newArray(int size) {
            return new Genre[size];
        }

    };

    public static Genre getGenreById(int id)
    {
        Realm realm = Realm.getDefaultInstance();
        Genre genre = realm.where(Genre.class)
                .equalTo("mId", id)
                .findFirst();

        return genre;
    }


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
