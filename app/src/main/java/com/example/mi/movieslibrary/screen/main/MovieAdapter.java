package com.example.mi.movieslibrary.screen.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mi.movieslibrary.R;
import com.example.mi.movieslibrary.models.content.Genre;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.utils.Images;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {
    Context context;
    List<Movie> mData;
    MovieItemClickListener movieItemClickListener;

    public MovieAdapter(Context context, List<Movie> mData) {
        this.context = context;
        this.mData = mData;

    }

    public MovieAdapter(Context context, List<Movie> mData, MovieItemClickListener listener) {
        this.context = context;
        this.mData = mData;
        movieItemClickListener = listener;
    }

    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_movie, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MyViewHolder myViewHolder, int i) {

        StringBuilder genreText = new StringBuilder();
        if(mData.get(i).getGenres()!=null) {
            for (Genre genre : mData.get(i).getGenres()) {
                genreText.append(genre.getmName()+"; ");
            }
            myViewHolder.TvJanr.setText(genreText);
        }
        myViewHolder.TvTitle.setText(mData.get(i).getmTitle());

        Images.loadMovie(myViewHolder.ImgMovie, mData.get(i), Images.WIDTH_185);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_movie_title)
        TextView TvTitle;
        @BindView(R.id.item_movie_janr)
        TextView TvJanr;
        @BindView(R.id.item_movie_img)
        ImageView ImgMovie;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    movieItemClickListener.onMovieClick(mData.get(getAdapterPosition()),ImgMovie);
                }
            });
        }
    }
}
