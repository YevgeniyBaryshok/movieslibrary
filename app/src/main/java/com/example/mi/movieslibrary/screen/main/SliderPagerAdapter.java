package com.example.mi.movieslibrary.screen.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mi.movieslibrary.R;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.utils.Images;

import java.util.List;

public class SliderPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<Movie> mSlideList;

    public SliderPagerAdapter(Context mContext, List<Movie> mSlideList) {
        this.mContext = mContext;
        this.mSlideList = mSlideList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View slideLayout = layoutInflater.inflate(R.layout.main_slide_item, null);

        ImageView slideImg = slideLayout.findViewById(R.id.main_slider_img);
        TextView slideTitle = slideLayout.findViewById(R.id.main_slider_title);

        Images.loadMovie(slideImg, mSlideList.get(position), Images.WIDTH_780);
        slideTitle.setText(mSlideList.get(position).getmTitle());

        container.addView(slideLayout);
        return slideLayout;
    }

    @Override
    public int getCount() {
        return mSlideList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }
}
