package com.example.mi.movieslibrary.screen.main;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mi.movieslibrary.R;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.screen.details.MovieDetailActivity;
import com.example.mi.movieslibrary.screen.general.LoadingDialog;
import com.example.mi.movieslibrary.screen.general.LoadingView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

public class MainActivity extends AppCompatActivity implements MainView {

    private Unbinder unbinder;
    private LoadingView mLoadingView;
    private MainPresenter mPresenter;

    @BindView(R.id.main_slider)
    ViewPager slidePager;

    @BindView(R.id.movies_rv)
    RecyclerView moviesRv;

    @BindView(R.id.main_slides_indicator)
    TabLayout indicator;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());

        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(this, getSupportLoaderManager());
        mPresenter = new MainPresenter(lifecycleHandler, this);
        mPresenter.init();


    }

//    //CHANGE TO PRESENTER
//    @Override
//    public void onMovieClick(Movie movie, ImageView movieImageView) {
//        // Here we send movie information to detail activity
//        //also we create the transition animation between the two activities
////
////        Intent intent = new Intent(this, MovieDetailActivity.class);
////        intent.putExtra("id", movie.getmId());
////        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,
////                movieImageView, "sharedName");
////        startActivity(intent, options.toBundle());
//        MovieDetailActivity.start(this, movie, movieImageView);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public void showPopular(@NonNull List<Movie> movies) {
        MovieAdapter movieAdapter = new MovieAdapter(this, movies, mPresenter);
        moviesRv.setAdapter(movieAdapter);
        moviesRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void showDetails(@NonNull Movie movie, @NonNull ImageView imageView) {
        MovieDetailActivity.start(this, movie, imageView);
    }

//    @Override
//    public void showDetails(@NonNull Movie movie) {
//
//    }

    @Override
    public void showLast(@NonNull Movie movies) {

    }
    @Override
    public void showRated(@NonNull List<Movie> movies) {
        SliderPagerAdapter pagerAdapter = new SliderPagerAdapter(this, movies);
        slidePager.setAdapter(pagerAdapter);

        indicator.setupWithViewPager(slidePager, true);
    }

    @Override
    public void showError() {

    }

    @Override
    public void showLoading() {
        mLoadingView.showLoading();
    }

    @Override
    public void hideLoading() {
        mLoadingView.hideLoading();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
