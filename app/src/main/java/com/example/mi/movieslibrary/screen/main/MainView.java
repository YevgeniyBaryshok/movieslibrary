package com.example.mi.movieslibrary.screen.main;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.screen.general.LoadingView;

import java.util.List;

public interface MainView extends LoadingView {
    void showPopular(@NonNull List<Movie> movies);
    void showDetails(@NonNull Movie movie, @NonNull ImageView imageView);
    void showRated(@NonNull List<Movie> movies);
    void showLast(@NonNull Movie movies);
    void showError();

}
