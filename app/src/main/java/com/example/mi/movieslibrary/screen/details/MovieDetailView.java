package com.example.mi.movieslibrary.screen.details;

import android.support.annotation.NonNull;

import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.screen.general.LoadingView;

import java.util.List;

public interface MovieDetailView extends LoadingView {
    void showDetails(@NonNull Movie movie);
//    void setFields();
//    void showCasts(@NonNull List<Cast> castList);
//    void showInfo(@NonNull List[] list);
}
