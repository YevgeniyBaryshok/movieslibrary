package com.example.mi.movieslibrary.utils;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.example.mi.movieslibrary.App;
import com.example.mi.movieslibrary.BuildConfig;
import com.example.mi.movieslibrary.models.content.Movie;
import com.squareup.picasso.Picasso;

public final class Images {

    public static final String WIDTH_185 = "w185";
    public static final String WIDTH_140 = "w140";
    public static final String WIDTH_780 = "w780";

    private Images() {
    }

    public static void loadMovie(@NonNull ImageView imageView, @NonNull Movie movie,
                                 @NonNull String size) {
        loadMovie(imageView, movie.getmPoster(), size);
    }

    public static void loadMovie(@NonNull ImageView imageView, @NonNull String posterPath,
                                 @NonNull String size) {
        String url = BuildConfig.IMAGES_BASE_URL + size + posterPath;
        Picasso.get()
                .load(url)
                .noFade()
                .into(imageView);
    }

    public static void fetch(@NonNull String posterPath, @NonNull String size) {
        String url = BuildConfig.IMAGES_BASE_URL + size + posterPath;
        Picasso.get()
                .load(url)
                .fetch();
    }
}