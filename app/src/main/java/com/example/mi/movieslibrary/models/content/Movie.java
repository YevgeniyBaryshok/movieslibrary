package com.example.mi.movieslibrary.models.content;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

import static com.example.mi.movieslibrary.models.content.Genre.getGenreById;

@Entity
public class Movie extends RealmObject implements Parcelable {

    @PrimaryKey
    @SerializedName("id")
    private int mId;
    @SerializedName("original_title")
    private String mTitle;
    @SerializedName("title")
    private String mLocalTitle;
    @SerializedName("original_language")
    private String mLanguage;
    @SerializedName("poster_path")
    private String mPoster;
    @SerializedName("backdrop_path")
    private String mBackdrop;
    private Boolean mAdult;
    @SerializedName("overview")
    private String mOverview;
    @SerializedName("release_date")
    private String mReleasedDate;
    @SerializedName("vote_average")
    private double mVoteAverage;

    @SerializedName("budget")
    private double mBudget;

    @io.realm.annotations.Ignore
    private int[] genre_ids;

    @io.realm.annotations.Ignore
    private List<Cast> casts;

    private String genresString;


    public Movie() {
    }

    public Movie(int mId, String mTitle, String mLocalTitle, String mLanguage, String mPoster, String mBackdrop, Boolean mAdult, String mOverview, String mReleasedDate, double mVoteAverage, int[] genre_ids) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mLocalTitle = mLocalTitle;
        this.mLanguage = mLanguage;
        this.mPoster = mPoster;
        this.mBackdrop = mBackdrop;
        this.mAdult = mAdult;
        this.mOverview = mOverview;
        this.mReleasedDate = mReleasedDate;
        this.mVoteAverage = mVoteAverage;
        this.genresString = Arrays.toString(genre_ids);
    }

    public Movie(Parcel in) {
        mId = in.readInt();
        mPoster = in.readString();
        mBackdrop = in.readString();
        mOverview = in.readString();
        mTitle = in.readString();
        mLocalTitle = in.readString();
        mAdult = in.readByte() != 0;
        mReleasedDate = in.readString();
        mVoteAverage = in.readDouble();
        genresString = in.readString();
    }

    public String getGenresString() {
        return genresString;
    }

    public void setGenresString(String genresString) {
        this.genresString = genresString;
    }


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public double getmBudget() {
        return mBudget;
    }

    public void setmBudget(double mBudget) {
        this.mBudget = mBudget;
    }

    @NonNull
    public List<Movie> convertGenreMovies(List<Movie> movies) {
        if (movies == null) {
            for (Movie item : movies) {

                item.setGenres(Arrays.toString(genre_ids));
            }
            return new ArrayList<>();
        }
        return movies;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        genresString = Arrays.toString(genre_ids);
        parcel.writeInt(mId);
        parcel.writeString(mPoster);
        parcel.writeString(mBackdrop);
        parcel.writeString(mLanguage);
        parcel.writeString(mOverview);
        parcel.writeString(mTitle);
        parcel.writeString(mLocalTitle);
        parcel.writeByte((byte) (mAdult ? 1 : 0));
        parcel.writeString(mReleasedDate);
        parcel.writeDouble(mVoteAverage);
        parcel.writeString(genresString);
    }

    public List<Cast> getCasts() {
        return casts;
    }

    public void setCasts(List<Cast> casts) {
        this.casts = casts;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {

        @NonNull
        @Override
        public Movie createFromParcel(Parcel parcel) {
            return new Movie(parcel);
        }

        @NonNull
        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }

    };

    public int[] getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(int[] genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getGenresText() {
        int[] genre_int = Arrays.stream(genresString.substring(1, genresString.length() - 1).split(","))
                .map(String::trim).mapToInt(Integer::parseInt).toArray();
        String result = "";
        StringBuilder genreText = new StringBuilder();

        for (int genre : genre_int) {
            genreText.append(getGenreById(genre).getmName() + "; ");
        }
        return String.valueOf(genreText);

    }

    public List<Genre> getGenres() {
        List<Genre> genreList = new ArrayList<Genre>();
        for (int genre_id : genre_ids) {
            genreList.add(getGenreById(genre_id));
        }
        return genreList;
    }


    public void setGenres(String genres) {
        this.genresString = genres;
    }


    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmPoster() {
        return mPoster;
    }

    public void setmPoster(String mPoster) {
        this.mPoster = mPoster;
    }

    public Boolean getmAdult() {
        return mAdult;
    }

    public void setmAdult(Boolean mAdult) {
        this.mAdult = mAdult;
    }

    public String getmOverview() {
        return mOverview;
    }

    public String getmLocalTitle() {
        return mLocalTitle;
    }

    public void setmLocalTitle(String mLocalTitle) {
        this.mLocalTitle = mLocalTitle;
    }

    public void setmOverview(String mOverview) {
        this.mOverview = mOverview;
    }

    public String getmReleasedDate() {
        return mReleasedDate;
    }

    public void setmReleasedDate(String mReleasedDate) {
        this.mReleasedDate = mReleasedDate;
    }

    public double getmVoteAverage() {
        return mVoteAverage;
    }

    public String getmBackdrop() {
        return mBackdrop;
    }

    public void setmBackdrop(String mBackdrop) {
        this.mBackdrop = mBackdrop;
    }

    public void setmVoteAverage(double mVoteAverage) {
        this.mVoteAverage = mVoteAverage;
    }

    public static Movie getMovieById(int id) {
        Movie movie;
        Realm realm = Realm.getDefaultInstance();
        Realm instance = null;
        try {
            instance = Realm.getDefaultInstance();
            realm.beginTransaction();
            movie = realm.where(Movie.class)
                    .equalTo("mId", id)
                    .findFirst();
            if (movie == null) {
                realm.cancelTransaction();
            }

            realm.commitTransaction();
        } catch (Throwable e) {
            if (instance != null && instance.isInTransaction()) {
                instance.cancelTransaction();
            }
            throw e;
        } finally {
            if (instance != null) {
                instance.close();
            }
        }
        return movie;
    }
}
