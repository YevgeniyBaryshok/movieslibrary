package com.example.mi.movieslibrary.models.content;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

import static com.example.mi.movieslibrary.models.content.Genre.getGenreById;

@Entity
public class Cast extends RealmObject implements Parcelable {

    @PrimaryKey
    @SerializedName("id")
    private int mId;
    @SerializedName("cast_id")
    private int mCastId;
    @SerializedName("character")
    private String mCharacter;
    @SerializedName("name")
    private String mName;
    @SerializedName("profile_path")
    private String mPhoto;


    public Cast() {
    }

    public Cast(int mId, int mCastId, String mCharacter, String mName, String mPhoto) {
        this.mId = mId;
        this.mCastId = mCastId;
        this.mCharacter = mCharacter;
        this.mName = mName;
        this.mPhoto = mPhoto;
    }

    public Cast(Parcel in) {
        mId = in.readInt();
        mCastId = in.readInt();
        mCharacter = in.readString();
        mName = in.readString();
        mPhoto = in.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(mId);
        parcel.writeInt(mCastId);
        parcel.writeString(mCharacter);
        parcel.writeString(mName);
        parcel.writeString(mPhoto);

    }

    public static final Creator<Cast> CREATOR = new Creator<Cast>() {

        @NonNull
        @Override
        public Cast createFromParcel(Parcel parcel) {
            return new Cast(parcel);
        }

        @NonNull
        @Override
        public Cast[] newArray(int size) {
            return new Cast[size];
        }

    };

    public String getmCharacter() {
        return mCharacter;
    }

    public void setmCharacter(String mCharacter) {
        this.mCharacter = mCharacter;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPhoto() {
        return mPhoto;
    }

    public void setmPhoto(String mPhoto) {
        this.mPhoto = mPhoto;
    }

    public static Cast getCastById(int id)
    {
        Realm realm = Realm.getDefaultInstance();
        Cast cast = realm.where(Cast.class)
                .equalTo("mId", id)
                .findFirst();

        return cast;
    }
}
