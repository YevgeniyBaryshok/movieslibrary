package com.example.mi.movieslibrary.models.response;

import android.support.annotation.NonNull;

import com.example.mi.movieslibrary.models.content.Genre;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GenreResponse {
    @SerializedName("genres")
    private List<Genre> mGenres;

    @NonNull
    public List<Genre> getGenres() {
        if (mGenres == null) {
            return new ArrayList<>();
        }
        return mGenres;
    }
}
