package com.example.mi.movieslibrary.screen.main;

import android.widget.ImageView;

import com.example.mi.movieslibrary.models.content.Movie;

public interface MovieItemClickListener {
    void onMovieClick(Movie movie, ImageView movieImage); //we will need the imageview to make the shared animation between two activities
}