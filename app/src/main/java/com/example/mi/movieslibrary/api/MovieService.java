package com.example.mi.movieslibrary.api;

import com.example.mi.movieslibrary.models.content.Genre;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.models.response.CreditsResponse;
import com.example.mi.movieslibrary.models.response.GenreResponse;
import com.example.mi.movieslibrary.models.response.MoviesResponse;

import java.util.List;

import retrofit2.http.Path;
import rx.Observable;

import retrofit2.http.GET;

public interface MovieService {

    @GET("movie/latest")
    Observable<Movie> latestMovie();

    @GET("movie/top_rated")
    Observable<MoviesResponse> topRated();

    @GET("movie/{movie_id}")
    Observable<Movie> movieDetails(@Path("movie_id") int id);

    @GET("movie/{movie_id}/credits")
    Observable<CreditsResponse> movieCasts(@Path("movie_id") int id);

    @GET("movie/popular")
    Observable<MoviesResponse> popularMovies();

    @GET("genre/movie/list")
    Observable<GenreResponse> genres();


}
