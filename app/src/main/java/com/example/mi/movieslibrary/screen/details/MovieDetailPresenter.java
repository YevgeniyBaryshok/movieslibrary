package com.example.mi.movieslibrary.screen.details;

import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.repository.RepositoryProvider;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Func2;

public class MovieDetailPresenter {
    private final MovieDetailView mView;

    public MovieDetailPresenter(MovieDetailView mView) {
        this.mView = mView;
    }

    public void init(int id) {

        Func2<Movie, List<Cast>, Movie> zipReviewVideo = (i, s) -> {
            i.setCasts(s);
            return i;
        };

        RepositoryProvider.provideGithubRepository()
                .movieDetails(id)
                .zipWith(RepositoryProvider.provideGithubRepository()
                                .casts(id),zipReviewVideo)
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(mView::showDetails);
    }
}
