package com.example.mi.movieslibrary.models.response;

import android.support.annotation.NonNull;

import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.models.content.Genre;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CreditsResponse {
    @SerializedName("cast")
    private List<Cast> mCasts;

    @NonNull
    public List<Cast> getCasts() {
        if (mCasts == null) {
            return new ArrayList<>();
        }
        return mCasts;
    }
}
