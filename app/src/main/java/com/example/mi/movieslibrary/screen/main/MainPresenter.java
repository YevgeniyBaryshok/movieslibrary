package com.example.mi.movieslibrary.screen.main;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.example.mi.movieslibrary.R;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.repository.RepositoryProvider;

import java.util.TimerTask;

import ru.arturvasilov.rxloader.LifecycleHandler;

public class MainPresenter implements MovieItemClickListener{
    private final LifecycleHandler mLifecycleHandler;
    private final MainView mView;

    public MainPresenter(@NonNull LifecycleHandler lifecycleHandler,
                         @NonNull MainView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }
    public void init() {

        RepositoryProvider.provideGithubRepository()
                .topRated()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(mView::showRated, throwable -> mView.showError());

        RepositoryProvider.provideGithubRepository()
                .genres()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(throwable -> mView.showError());

        RepositoryProvider.provideGithubRepository()
                .popularMovies()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(mView::showPopular, throwable -> mView.showError());


    }

//    public void onMovieClick(@NonNull Movie movie) {
//
//    }

    @Override
    public void onMovieClick(Movie movie, ImageView movieImage) {
        mView.showDetails(movie, movieImage);
    }


//    class SliderTimer extends TimerTask {
//        @Override
//        public void run() {
//            MainActivity.this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    if (slidePager.getCurrentItem() < listSlides.size() - 1) {
//                        slidePager.setCurrentItem(slidePager.getCurrentItem() + 1);
//                    } else {
//                        slidePager.setCurrentItem(0);
//                    }
//                }
//            });
//        }
//    }
}
