package com.example.mi.movieslibrary.models.response;

import android.support.annotation.NonNull;

import com.example.mi.movieslibrary.models.content.Movie;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class MoviesResponse {
    @SerializedName("results")
    private List<Movie> mMovies;

    @NonNull
    public List<Movie> getMovies() {
        if (mMovies == null) {
            return new ArrayList<>();
        }
        return mMovies;
    }

}
