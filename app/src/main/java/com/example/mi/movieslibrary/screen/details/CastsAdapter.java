package com.example.mi.movieslibrary.screen.details;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mi.movieslibrary.R;
import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.utils.Images;

import java.util.List;

public class CastsAdapter extends RecyclerView.Adapter<CastsAdapter.CastViewHolder> {

    Context mContext;
    List<Cast> mData;

    public CastsAdapter(Context mContext, List<Cast> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_cast, viewGroup, false);
        return new CastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHolder castViewHolder, int i) {
        castViewHolder.name.setText(mData.get(i).getmName());
        castViewHolder.role.setText(mData.get(i).getmCharacter());
        Images.loadMovie(castViewHolder.img, mData.get(i).getmPhoto(), Images.WIDTH_185);
//        Glide.with(mContext).load(mData.get(i).getImg_link()).into(castViewHolder.img);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class CastViewHolder extends RecyclerView.ViewHolder{

        ImageView img;
        TextView name;
        TextView role;

        public CastViewHolder(@NonNull View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.cast_img);
            name=itemView.findViewById(R.id.cast_name);
            role=itemView.findViewById(R.id.cast_character);
        }
    }
}
