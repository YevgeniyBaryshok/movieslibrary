package com.example.mi.movieslibrary.utils;

import io.realm.RealmObject;

public class RealmInt extends RealmObject {
    private int val;

    public RealmInt() {
    }

    public RealmInt(int val) {
        this.val = val;
    }

    // Getters and setters
}