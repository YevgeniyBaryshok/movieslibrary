package com.example.mi.movieslibrary.screen.general;

public interface LoadingView {

    void showLoading();
    void hideLoading();

}