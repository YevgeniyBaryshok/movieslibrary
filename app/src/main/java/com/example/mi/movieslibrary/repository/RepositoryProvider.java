package com.example.mi.movieslibrary.repository;

import android.support.annotation.NonNull;

public class RepositoryProvider {
    private static MovieRepository sMovieRepository;

    @NonNull
    public static MovieRepository provideGithubRepository() {
        if (sMovieRepository == null) {
            sMovieRepository = new DefaultMovieRepository();
        }
        return sMovieRepository;
    }
}
