package com.example.mi.movieslibrary.screen.details;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mi.movieslibrary.R;
import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.models.content.Genre;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.repository.RepositoryProvider;
import com.example.mi.movieslibrary.screen.general.LoadingDialog;
import com.example.mi.movieslibrary.screen.general.LoadingView;
import com.example.mi.movieslibrary.screen.main.MainActivity;
import com.example.mi.movieslibrary.screen.main.MainView;
import com.example.mi.movieslibrary.utils.Images;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

import static com.example.mi.movieslibrary.models.content.Movie.getMovieById;

public class MovieDetailActivity extends AppCompatActivity implements MovieDetailView {
    private Unbinder unbinder;
    //    private CastAdapter castAdapter;\
    private LoadingView mLoadingView;
    private MovieDetailPresenter mPresenter;
    @BindView(R.id.detail_movie_img)
    ImageView MovieThumbnailImg;

    @BindView(R.id.detail_movie_cover)
    ImageView MovieCoverImg;

    @BindView(R.id.detail_movie_title)
    TextView tv_title;

    @BindView(R.id.detail_movie_original_title)
    TextView tv_original_title;

    @BindView(R.id.detail_movie_budget)
    TextView tv_budget;

    @BindView(R.id.detail_movie_desc)
    TextView tv_description;

    @BindView(R.id.detail_movie_genres)
    TextView tv_genres;

    @BindView(R.id.rv_cast)
    RecyclerView RvCast;

    @BindView(R.id.play_fab)
    FloatingActionButton play_fab;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public static void start(@NonNull Activity activity, @NonNull Movie movie, ImageView movieImageView) {
        Intent intent = new Intent(activity, MovieDetailActivity.class);
        intent.putExtra("id", movie.getmId());
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity,
                movieImageView, "sharedName");
        activity.startActivity(intent,options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());

        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(this, getSupportLoaderManager());
        mPresenter = new MovieDetailPresenter(this);

        iniViews();

    }

    void iniViews() {

        int id = Objects.requireNonNull(getIntent().getExtras()).getInt("id");

        Movie movie = getMovieById(id);
        mPresenter.init(id);

        String movieTitle = movie.getmLocalTitle();
        String movieOriginalTitle = movie.getmTitle();
        String movieDescription = movie.getmOverview();

//       tv_genres.setText(movie.getGenresText());

        Images.loadMovie(MovieThumbnailImg, movie.getmPoster(), Images.WIDTH_185);
        Images.loadMovie(MovieCoverImg, movie.getmBackdrop(), Images.WIDTH_780);

        tv_title.setText(movieTitle);
        tv_original_title.setText(movieOriginalTitle);
        tv_description.setText(movieDescription);

        getSupportActionBar().setTitle(movieTitle);



        //setup animation

        MovieCoverImg.setAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_animation));
        play_fab.setAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_animation));
//        tv_description.setText(movieDescr);
    }

    @Override
    public void showLoading() {
        mLoadingView.showLoading();
    }

    @Override
    public void hideLoading() {
        mLoadingView.hideLoading();
    }

    @Override
    public void showDetails(@NonNull Movie movie) {
        if (movie.getmBudget() != 0) {
            tv_budget.setText(String.valueOf(movie.getmBudget()));
        }
        CastsAdapter castsAdapter = new CastsAdapter(this, movie.getCasts());
        RvCast.setAdapter(castsAdapter);
        RvCast.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }
//    @Override
//    public void showCasts(@NonNull List<Cast> casts) {
//
//    }
}
