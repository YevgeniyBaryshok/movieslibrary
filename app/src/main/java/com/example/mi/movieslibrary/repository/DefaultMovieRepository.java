package com.example.mi.movieslibrary.repository;

import android.support.annotation.NonNull;
import com.example.mi.movieslibrary.api.ApiFactory;
import ru.arturvasilov.rxloader.RxUtils;

import com.example.mi.movieslibrary.models.content.Cast;
import com.example.mi.movieslibrary.models.content.Genre;
import com.example.mi.movieslibrary.models.content.Movie;
import com.example.mi.movieslibrary.models.response.CreditsResponse;
import com.example.mi.movieslibrary.models.response.GenreResponse;
import com.example.mi.movieslibrary.models.response.MoviesResponse;
//import com.philosophicalhacker.lib.RxLoader;

import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Func1;

public class DefaultMovieRepository implements MovieRepository {
    @NonNull
    @Override
    public Observable<List<Movie>> popularMovies() {
//        final RxLoader rxLoader = new RxLoader(this, getSupportLoaderManager());
        return ApiFactory.getMoviesService()
                .popularMovies()
                .map(MoviesResponse::getMovies)
                .concatMap((Func1<List<Movie>, Observable<Movie>>) Observable::from)
                .take(5)
                .filter((mov) -> {
                    mov.setGenres(Arrays.toString(mov.getGenre_ids()));
                    return true;
                })
                .toList()
                .flatMap(movies -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {

                        realm.delete(Movie.class);
                        realm.insert(movies);
//                        realm.insert(movies);
                    });
                    return Observable.just(movies);
                })
                .onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Movie> movies = realm.where(Movie.class).findAll();
                    return Observable.just(realm.copyFromRealm(movies));
                })
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Movie>> topRated() {
//        final RxLoader rxLoader = new RxLoader(this, getSupportLoaderManager());
        return ApiFactory.getMoviesService()
                .topRated()
                .map(MoviesResponse::getMovies)
                .concatMap((Func1<List<Movie>, Observable<Movie>>) movies -> Observable.from(movies))
                .take(3)
                .toList()
                .flatMap(movies -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {
//                        realm.delete(Movie.class);
                        realm.insertOrUpdate(movies);
                    });
                    return Observable.just(movies);
                })
                .onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Movie> movies = realm.where(Movie.class).findAll();
                    return Observable.just(realm.copyFromRealm(movies));
                })
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<Movie> movieDetails(int id) {
        return ApiFactory.getMoviesService()
                .movieDetails(id)
                .flatMap(movie -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        Movie movie1 = realm.where(Movie.class)
                                .equalTo("mId", id)
                                .findFirst();
//                        realm.beginTransaction();
                        if (movie1 == null) {
                            realm.insert(movie);
                            // set the fields here
                        } else {
                            movie1.setmBudget(movie.getmBudget());
                        }
//                        realm.commitTransaction();
                    });
                    return Observable.just(movie);
                }).onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    Movie movie = realm.where(Movie.class).findFirst();
                    return Observable.just(realm.copyFromRealm(movie));
                })
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Genre>> genres() {
        return ApiFactory.getMoviesService()
                .genres()
                .map(GenreResponse::getGenres)
                .flatMap(genres -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        realm.delete(Genre.class);
                        realm.insert(genres);
                    });
                    return Observable.just(genres);
                })
                .onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Genre> genres = realm.where(Genre.class).findAll();
                    return Observable.just(realm.copyFromRealm(genres));
                })
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Cast>> casts(int id) {
        return ApiFactory.getMoviesService()
                .movieCasts(id)
                .map(CreditsResponse::getCasts)
                .flatMap(casts -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        realm.delete(Cast.class);
                        realm.insert(casts);
                    });
                    return Observable.just(casts);
                })
                .onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Cast> casts = realm.where(Cast.class).findAll();
                    return Observable.just(realm.copyFromRealm(casts));
                })
                .compose(RxUtils.async());
    }
}
